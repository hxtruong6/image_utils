# Blur image detection

Using:

```
        from blur_detection import is_blur_image
        is_blur_image(image_path) #return True/False
```