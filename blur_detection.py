import cv2


def variance_of_laplacian(image):
    # compute the Laplacian of the image and then return the focus
    # measure, which is simply the variance of the Laplacian
    return cv2.Laplacian(image, cv2.CV_64F).var()


"""
blur_threshold: default = 150. Change value to optimize in specific dataset
"""


def is_blur_image(image_path: str, blur_threshold=150):
    image = cv2.imread(image_path)
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    fm = variance_of_laplacian(gray)
    print(f"Var {image_path} = {fm}")
    # if the focus measure is less than the supplied threshold,
    # then the image should be considered "blurry"
    if fm < blur_threshold:
        return True
    return False
