# Press the green button in the gutter to run the script.
from blur_detection import is_blur_image

if __name__ == '__main__':
    imgs = ["test_image/1.jpg", "test_image/2.jpg", "test_image/3.png", ]
    for img in imgs:
        is_blur_image(img)

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
